﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Managment;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Roles = new List<Role>()
                {
                    Roles.FirstOrDefault(x => x.Name == "Admin")
                },
                AppliedPromocodesCount = 5
            },
            new Employee
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Roles = new List<Role>()
                {
                    Roles.FirstOrDefault(x => x.Name == "PartnerManager")
                },
                AppliedPromocodesCount = 10
            }
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            new Customer
            {
                Id = Guid.Parse("ebde3c86-9597-46a9-8cd1-f44fba973f21"),
                FirstName = "Александр",
                LastName = "Черный",
                Email = "alchor@gmail.com"
            },
            new Customer
            {
                Id = Guid.Parse("f3e387fe-5aba-4d59-9feb-b2a3141f75af"),
                FirstName = "Вениамин",
                LastName = "Смехов",
                Email = "venya@mail.com"
            }
        };

        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference
            {
                Id = Guid.Parse("842d6ae0-e0d3-4ae1-8596-81dc1511088b"),
                Name = "Спорт"
            },
            new Preference
            {
                Id = Guid.Parse("3d670786-555e-4c2b-95bc-a4c8559cd433"),
                Name = "Кино"
            },
            new Preference
            {
                Id = Guid.Parse("841ec920-ceba-44f9-9b47-ec096b328534"),
                Name = "Обучение"
            }
        };


        public static IEnumerable<CustomerPreference> CustomerPreferences => new List<CustomerPreference>
        {
            new CustomerPreference
            {
                Id = Guid.NewGuid(),
                CustomerId =  Guid.Parse("ebde3c86-9597-46a9-8cd1-f44fba973f21"),
                PreferenceId = Guid.Parse("3d670786-555e-4c2b-95bc-a4c8559cd433")
            },
            new CustomerPreference
            {
                Id = Guid.NewGuid(),
                CustomerId =  Guid.Parse("f3e387fe-5aba-4d59-9feb-b2a3141f75af"),
                PreferenceId = Guid.Parse("841EC920-CEBA-44F9-9B47-EC096B328534")
            },
            new CustomerPreference
            {
                Id = Guid.NewGuid(),
                CustomerId =  Guid.Parse("f3e387fe-5aba-4d59-9feb-b2a3141f75af"),
                PreferenceId = Guid.Parse("842D6AE0-E0D3-4AE1-8596-81DC1511088B")
            },
            new CustomerPreference
            {
                Id = Guid.NewGuid(),
                CustomerId =  Guid.Parse("f3e387fe-5aba-4d59-9feb-b2a3141f75af"),
                PreferenceId = Guid.Parse("3d670786-555e-4c2b-95bc-a4c8559cd433")
            }
        };

        public static IEnumerable<PromoCode> Promocodes => new List<PromoCode>()
        {
            new PromoCode
            {
                Id = Guid.Parse("3b90b7aa-5ee0-4cde-ad20-6a39ddca5e91"),
                Code = "SPORT_ALL",
                StartDate = new DateTime(2021, 12, 01),
                EndDate = new DateTime(2021, 12, 30),
                PartnerManagerId = Guid.Parse("451533D5-D8D5-4A11-9C7B-EB9F14E1A32F"),
                PartnerName = "FitnessHouse",
                PreferenceId = Guid.Parse("842D6AE0-E0D3-4AE1-8596-81DC1511088B"),
                Comments = "FitnessHouse promocode for December",
                CustomerId = Guid.Parse("ebde3c86-9597-46a9-8cd1-f44fba973f21")
            },
            new PromoCode
            {
                Id = Guid.Parse("3d78be83-4972-49c9-bc93-6cf9b4c34491"),
                Code = "OTUS_DISCOUNT",
                StartDate = new DateTime(2021, 12, 01),
                EndDate = new DateTime(2021, 12, 31),
                PartnerManagerId = Guid.Parse("F766E2BF-340A-46EA-BFF3-F1700B435895"),
                PartnerName = "OTUS",
                PreferenceId = Guid.Parse("841EC920-CEBA-44F9-9B47-EC096B328534"),
                Comments = "5 sessions free of charge",
                CustomerId = Guid.Parse("f3e387fe-5aba-4d59-9feb-b2a3141f75af")
            },
            new PromoCode
            {
                Id = Guid.Parse("786e4ee6-72f1-46f1-92a9-992db09212c5"),
                Code = "CINEMA_GO",
                StartDate = new DateTime(2021, 12, 14),
                EndDate = new DateTime(2021, 12, 20),
                PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PartnerName = "Cinema Park",
                PreferenceId = Guid.Parse("3d670786-555e-4c2b-95bc-a4c8559cd433"),
                Comments = "Cinema Park invites you and your family",
                CustomerId = Guid.Parse("f3e387fe-5aba-4d59-9feb-b2a3141f75af")
            }
        };
    }
}