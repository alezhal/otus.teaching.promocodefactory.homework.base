﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T: BaseEntity
    {
        private readonly DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _dataContext.Set<T>().ToListAsync();
            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        public async Task<Guid> AddAsync(T entity)
        {
            if(entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
            var entities = await _dataContext.Set<T>().FirstOrDefaultAsync(x => entity.Id == x.Id);
            return entities.Id;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            _dataContext.Update(entity);
            await _dataContext.SaveChangesAsync();

            return entity;
        }

        public async Task<string> RemoveAsync(Guid id)
        {
            string result = "OK";
            if (id == null)
            {
                throw new ArgumentNullException("id");
            }

            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => id == x.Id);
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();

            return result;
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            if (entities == null)
            {
                throw new ArgumentNullException("entity");
            }

            await _dataContext.Set<T>().AddRangeAsync(entities);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteRangeAsync(IEnumerable<T> entities)
        {
            _dataContext.RemoveRange(entities);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            if (ids == null)
            {
                return null;
            }

            var entities = await _dataContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<T> GetFirstWhereAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> GetWhereAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dataContext.Set<T>().Where(predicate).ToListAsync();
        }
    }
}
