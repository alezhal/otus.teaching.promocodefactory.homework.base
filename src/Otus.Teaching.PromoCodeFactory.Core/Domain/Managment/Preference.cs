﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Managment
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<CustomerPreference> CustomersPreference { get; set; }
    }
}
