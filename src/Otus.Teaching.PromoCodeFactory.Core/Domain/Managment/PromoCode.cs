﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Managment
{
    public class PromoCode : BaseEntity
    {
        [MaxLength(50)]
        public string Code { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public Guid? PreferenceId { get; set; }

        public virtual Preference Preference { get; set; }

        public Guid? PartnerManagerId { get; set; }

        public virtual Employee PartnerManager { get; set; }

        [MaxLength(50)]
        public string PartnerName { get; set; }

        [MaxLength(100)]
        public string Comments { get; set; }
    }
}
