﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Managment
{
    public class Customer : BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(150)]
        public string Email { get; set; }

        public string Patronymic { get; set; }

        public virtual ICollection<CustomerPreference> CustomersPreferences { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}
