﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public List<RoleItemResponse> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }

        public EmployeeResponse()
        {

        }

        public EmployeeResponse(Employee employee)
        {
            Id = employee.Id;
            Email = employee.Email;
            FirstName = employee.FirstName;
            LastName = employee.LastName;
            FullName = employee.FullName;
            AppliedPromocodesCount = employee.AppliedPromocodesCount;
            Roles = employee.Roles?.Select(x => new RoleItemResponse()
            {
                Name = x.Name,
                Description = x.Description
            }).ToList();
        }
    }
}