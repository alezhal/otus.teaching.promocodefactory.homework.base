﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodesRequest
    {
        public string PromoCode { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PartnerName { get; set; }

        public string Preference { get; set; }

        public string Comments { get; set; }
    }
}
