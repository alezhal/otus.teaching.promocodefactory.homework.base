﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Managment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        public virtual List<PromoCodeShortResponse> PromoCodes { get; set; }

        public virtual List<PreferenceShortResponse> CustomersPreferences { get; set; }

        public CustomerResponse()
        {

        }

        public CustomerResponse(Customer customer)
        {
            Id = customer.Id;
            Email = customer.Email;
            FirstName = customer.FirstName;
            LastName = customer.LastName;

            CustomersPreferences = customer.CustomersPreferences?.Select(x => new PreferenceShortResponse()
            {
                Id = x.Preference.Id,
                Name = x.Preference.Name

            }).ToList();
            PromoCodes = customer.PromoCodes?.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                StartDate = x.StartDate.ToString("dd-MMM-yyyy"),
                EndDate = x.EndDate.ToString("dd-MMM-yyyy"),
                Code = x.Code,
                PartnerName = x.PartnerName,
                Comments = x.Comments,
            }).ToList();
        }
    }
}
