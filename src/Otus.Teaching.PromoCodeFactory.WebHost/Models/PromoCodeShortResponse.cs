﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Managment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeShortResponse
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string PartnerName { get; set; }

        public string CustomerEmail { get; set; }

        public string Comments { get; set; }

        public PromoCodeShortResponse()
        {

        }

        public PromoCodeShortResponse(PromoCode promocode)
        {
            Id = promocode.Id;
            StartDate = promocode.StartDate.ToString("dd.MM.yyyy");
            Code = promocode.Code;
            EndDate = promocode.EndDate.ToString("dd.MM.yyyy");
            PartnerName = promocode.PartnerName;
            CustomerEmail = promocode.Customer.Email;
            Comments = promocode.Comments;
        }
    }
}
