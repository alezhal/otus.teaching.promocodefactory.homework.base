﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Managment;
using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceShortResponse
    {
        public Guid Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        public PreferenceShortResponse()
        {

        }

        public PreferenceShortResponse(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }
    }
}
