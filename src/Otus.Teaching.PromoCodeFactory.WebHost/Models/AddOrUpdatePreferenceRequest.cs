﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class AddOrUpdatePreferenceRequest
    {
        public string Name { get; set; }
    }
}
