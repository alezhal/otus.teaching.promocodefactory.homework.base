﻿using System;
using System.Collections.Generic;
namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class AddOrUpdatePromoCodeRequest
    {
        public string Code { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PreferenceName { get; set; }

        public Guid CustomerId { get; set; }

        public Guid? PartnerManagerId { get; set; }

        public string PartnerName { get; set; }

        public string Comments { get; set; }
    }
}
