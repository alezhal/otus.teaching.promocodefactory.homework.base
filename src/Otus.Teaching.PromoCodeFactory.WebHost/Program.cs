using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { 
                    webBuilder.UseStartup<Startup>();

                    webBuilder.ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        config.AddJsonFile("project.json", false, true);
                        config.AddXmlFile("azXmlCustomConfg.xml", false, true);
                        config.AddJsonFile("azJsonCustomConfg.json", false, true);
                        config.AddInMemoryCollection(new Dictionary<string, string>
                        {
                            {"StartedAtUtc", DateTime.UtcNow.ToString("O") }
                        });
                    });
                });
    }
}