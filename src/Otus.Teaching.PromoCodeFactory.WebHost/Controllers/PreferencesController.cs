﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Managment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceShortResponse>> GetPreferenceAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferencesModelList = preferences.Select(x =>
            new PreferenceShortResponse
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            if (preferencesModelList.Count > 0)
                return Ok(preferencesModelList);
            else
                return StatusCode(StatusCodes.Status200OK, "No preference has been created yet");
        }

        /// <summary>
        /// Получить предпочтения по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceShortResponse>> GetPreferenceByIdAsync(Guid id)
        {
            try
            {
                var preference = await _preferenceRepository.GetByIdAsync(id);

                if (preference == null)
                    return NotFound();

                var preferenceModel = new PreferenceShortResponse(preference);

                if (preferenceModel == null)
                {
                    return NotFound();
                }

                return preferenceModel;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the memory");
            }
        }

        /// <summary>
        /// Добавить препочтение
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Guid>> AddPreferenceAsync(AddOrUpdatePreferenceRequest request)
        {
            try
            {
                if (request == null)
                    return BadRequest();

                var preference = new Preference
                {
                    Id = Guid.NewGuid(),
                    Name = request.Name
                };

                var newpreference = await _preferenceRepository.AddAsync(preference);

                return await Task.FromResult(newpreference);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error creating new preference");
            }
        }

        /// <summary>
        /// Отредактировать предпочтение
        /// </summary>
        [HttpPut("{id:Guid}")]
        public async Task<ActionResult<Preference>> UpdatePreferenceAsync(Guid id, Preference preference)
        {
            try
            {
                if (id != preference.Id)
                    return BadRequest("Preference ID mismatch");

                var preferenceToUpdate = await _preferenceRepository.GetByIdAsync(preference.Id);

                if (preferenceToUpdate == null)
                {
                    return NotFound($"Preference with Id: {preference.Id} not found");
                }
                
                return await _preferenceRepository.UpdateAsync(preference);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error updating preference");
            }
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        [HttpDelete("{id:Guid}")]
        public async Task<ActionResult<string>> RemovePreferenceAsync(Guid id)
        {
            try
            {
                var preferenceToDelete = await _preferenceRepository.GetByIdAsync(id);

                if (preferenceToDelete == null)
                {
                    return NotFound($"Preference with Id: {id} not found");
                }

                var result = await _preferenceRepository.RemoveAsync(id);

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting preference");
            }
        }
    }
}
