﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController: ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository; 
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        //public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        public async Task<ActionResult<EmployeeShortResponse>> GetEmployeesAsync()
        {
            try
            {
                var employees = await _employeeRepository.GetAllAsync();

                var employeesModelList = employees.Select(x =>
                    new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

                return Ok(employeesModelList);
            }
            catch(Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the memory");
            }
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            try
            {
                var employee = await _employeeRepository.GetByIdAsync(id);

                if (employee == null)
                    return NotFound();

                //var employeeModel = new EmployeeResponse()
                //{
                //    Id = employee.Id,
                //    Email = employee.Email,
                //    Roles = employee.Roles?.Select(x => new RoleItemResponse()
                //    {
                //        Name = x.Name,
                //        Description = x.Description
                //    }).ToList(),
                //    FullName = employee.FullName,
                //    AppliedPromocodesCount = employee.AppliedPromocodesCount
                //};

                var employeeModel = new EmployeeResponse(employee);

                if (employeeModel == null)
                {
                    return NotFound();
                }

                return employeeModel;
            }
            catch(Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the memory");
            }
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Guid>> AddEmployeeAsync(Employee employee)
        {
            try
            {
                if (employee == null)
                    return BadRequest();

                employee.Id = Guid.NewGuid();
                var newEmployee = await _employeeRepository.AddAsync(employee);
                
                return await Task.FromResult(newEmployee);
            }
            catch(Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error creating new employee");
            }
        }

        /// <summary>
        /// Отредактировать сотрудника
        /// </summary>
        [HttpPut("{id:Guid}")]
        public async Task<ActionResult<Employee>> UpdateEmployeeAsync(Guid id, Employee employee)
        {
            try
            {
                if (id == null)
                    return BadRequest();

                if (id != employee.Id)
                    return BadRequest("Employee ID mismatch");

                return await _employeeRepository.UpdateAsync(employee);
            }
            catch(Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error updating employee");
            }
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        [HttpDelete("{id:Guid}")]
        public async Task<ActionResult<string>> RemoveEmployeeAsync(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest();

                return await _employeeRepository.RemoveAsync(id);
            }
            catch(Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting employee");
            }
        }
    }
}