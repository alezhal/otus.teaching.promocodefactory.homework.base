﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Managment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех покупателей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            try
            {
                var customers = await _customerRepository.GetAllAsync();
                var customerModelList = customers.Select(x =>
                    new CustomerShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

                if (customerModelList.Count > 0)
                    return Ok(customerModelList);
                else
                    return StatusCode(StatusCodes.Status200OK,"No customer has been created yet");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }

        /// <summary>
        /// Получить данные покупателя по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerByIdAsync(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest();

                var customer = await _customerRepository.GetByIdAsync(id);

                if (customer == null)
                    return NotFound();

                var customerModel = new CustomerResponse(customer);

                if (customerModel == null)
                {
                    return NotFound();
                }

                return customerModel;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }

        /// <summary>
        /// Добавить покупателя
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Guid>> AddCustomerAsync(AddOrUpdateCustomerRequest request)
        {
            try
            {
                if (request == null)
                    return BadRequest();

                var preferences =  await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

                var customer = new Customer
                {
                    Id = Guid.NewGuid(),
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                    CustomersPreferences = preferences
                    ?.Select(x => new CustomerPreference
                    {
                        PreferenceId = x.Id
                    }).ToList()
                };

                var newCustomer = await _customerRepository.AddAsync(customer);

                return await Task.FromResult(newCustomer);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error creating new customer");
            }
        }

        /// <summary>
        /// Отредактировать покупателя
        /// </summary>
        [HttpPut("{id:Guid}")]
        public async Task<ActionResult<Customer>> UpdateCustomerAsync(Guid id, AddOrUpdateCustomerRequest request)
        {
            try
            {
                if(id == null)
                    return BadRequest();

                var customer = await _customerRepository.GetByIdAsync(id);

                if (customer == null)
                    return NotFound();

                if (id != customer.Id)
                    return BadRequest("Customer ID mismatch");


                var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

                customer.Email = request.Email;
                customer.LastName = request.LastName;
                customer.CustomersPreferences = preferences
                    ?.Select(x => new CustomerPreference
                    {
                        PreferenceId = x.Id,
                        Preference = new Preference
                        {
                            Id = x.Id,
                            Name = x.Name
                        }
                    }).ToList();
                
                return await _customerRepository.UpdateAsync(customer);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error updating customer");
            }
        }

        /// <summary>
        /// Удалить покупателя
        /// </summary>
        [HttpDelete("{id:Guid}")]
        public async Task<ActionResult<string>> RemoveCustomerAsync(Guid id)
        {
            try
            {
                var result = await _customerRepository.RemoveAsync(id);
                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data");
            }
        }
    }
}
