﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Managment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository
            , IRepository<Preference> preferenceRepository
            , IRepository<Customer> customerRepository
            , IRepository<Employee> employeeRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns>Список текущих выданных промокодов</returns>
        [HttpGet]
        public async Task<ActionResult<PromoCode>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
         
            var promoCodesModelList = promoCodes.Select(x =>
            new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                StartDate = x.StartDate.ToString("dd.MM.yyyy"),
                EndDate = x.EndDate.ToString("dd.MM.yyyy"),
                PartnerName = x.PartnerName,
                Comments = x.Comments
            }).ToList();

            if (promoCodesModelList.Count > 0)
                return Ok(promoCodesModelList);
            else
                return StatusCode(StatusCodes.Status200OK, "No promocode has been created yet");
        }


        /// <summary>
        /// Получить промокод по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PromoCode>> GetCustomerByIdAsync(Guid id)
        {
            try
            {
                if (id == null)
                    return BadRequest();

                var promocode = await _promoCodeRepository.GetByIdAsync(id);

                if (promocode == null)
                    return NotFound();

                var promoCodeModel = new PromoCodeShortResponse(promocode);

                if (promoCodeModel == null)
                {
                    return NotFound();
                }

                return Ok(promoCodeModel);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data");
            }
        }


        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Результат выдачи промокодов</returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomerWithPreferenceAsync(GivePromoCodesRequest request)
        {
            if (request == null)
                return BadRequest();
            try
            {
                var preferences = await _preferenceRepository.GetAllAsync();
                //var preference = preferences.FirstOrDefault(x => string.Equals(x.Name, request.Preference, StringComparison.OrdinalIgnoreCase));
                var preference = await _preferenceRepository.GetFirstWhereAsync(x => x.Name.Equals(request.Preference));

                if (preference == null)
                    return NotFound();


                await _promoCodeRepository.AddRangeAsync(
                    preference.CustomersPreference.Select(x => new PromoCode
                    {
                        Id = Guid.NewGuid(),
                        Code = request.PromoCode,
                        PartnerName = request.PartnerName,
                        Comments = request.Comments,
                        StartDate = request.StartDate,
                        EndDate = request.EndDate,
                        Preference = preference,
                        Customer = x.Customer
                    }
                    ));
            }
            catch(Exception)
            {

            }
            return Ok();
        }

        /// <summary>
        /// Добавить промокод
        /// </summary>
        [HttpPut]
        public async Task<ActionResult<Guid>> AddPromoCodeAsync(AddOrUpdatePromoCodeRequest request)
        {
            try
            {
                if (request == null)
                    return BadRequest();

                var preferences = await _preferenceRepository.GetAllAsync();
                var customers = await _customerRepository.GetAllAsync();
                var employees = await _employeeRepository.GetAllAsync();

                var promoCode = new PromoCode
                {
                    Id = Guid.NewGuid(),
                    Code = request.Code,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate,
                    PreferenceId = preferences.FirstOrDefault(x => x.Name == request.PreferenceName).Id,
                    CustomerId = request.CustomerId,
                    PartnerManagerId = request.PartnerManagerId,
                    PartnerName = request.PartnerName,
                    Comments = request.Comments
                };

                var newPromoCode = await _promoCodeRepository.AddAsync(promoCode);

                return await Task.FromResult(newPromoCode);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error creating new promocode");
            }
        }

        /// <summary>
        /// Удалить промокод
        /// </summary>
        [HttpDelete("{id:Guid}")]
        public async Task<ActionResult<string>> RemovePromoCodeAsync(Guid id)
        {
            try
            {
                var result = await _promoCodeRepository.RemoveAsync(id);
                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting data");
            }
        }
    }
}
